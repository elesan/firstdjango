fifth_letter = "MONTY"[4]
print fifth_letter
# Y

//string
len()
lower()
upper()
str()

parrot = "Norwegian Blue"
print len(parrot)
# 14

parrot = "Norwegian Blue"
print parrot.lower()

pi = 3.14
print str(pi)

////////////
name = raw_input("What is your name?")
quest = raw_input("What is your quest?")
color = raw_input("What is your favorite color?")

print "Ah, so your name is %s, your quest is %s, " \
"and your favorite color is %s." % (name, quest, color)


The datetime Library

from datetime import datetime
now = datetime.now()

print '%s/%s/%s %s:%s:%s' % (now.month, now.day, now.year,now.hour, now.minute, now.second)

if else
# Assign True or False as appropriate on the lines below!
# Set this to True if 17 < 328 or to False if it is not.
bool_one = True   # We did this one for you!
# Set this to True if 100 == (2 * 50) or to False otherwise.
bool_two = True if 100 == (2 * 50) else False
# Set this to True if 19 <= 19 or to False if it is not.
bool_three = True if 19 <= 19 else False
# Set this to True if -22 >= -18 or to False if it is not.
bool_four = True if -22 >= -18 else False
# Set this to True if 99 != (98 + 1) or to False otherwise.
bool_five = True if 99 != (98 + 1) else False

bool_one = 2**3 == 108 % 100 or 'Cleese' == 'King Arthur'
bool_two = True or False
bool_three = 100**0.5 >= 50 or False
bool_four = True or True
bool_five = 1**100 == 100**1 or 3 * 2 * 1 != 3 + 2 + 1

bool_one = not True
bool_two = not 3**4 < 4**3
bool_three = not 10 % 3 <= 10 % 2
bool_four = not 3**2 + 4**2 != 5**2
bool_five = not not False

bool_one = False or not True and True
bool_two = False and not True or True
bool_three = False and not True or True
bool_four = True and not (False or False)
bool_five = False or not (True and True)

def using_control_once():
    if True:
        return "Success #1"

def using_control_again():
    if True:
        return "Success #2"

print using_control_once()
print using_control_again()

def greater_less_equal_5(answer):
    if answer > 5:
        return 1
    elif answer < 5:          
        return -1
    else:
        return 0
        
print greater_less_equal_5(4)
print greater_less_equal_5(5)
print greater_less_equal_5(6)

def the_flying_circus():
    if 45 < 5:
        return False

    elif 1 > 0:
        return True
        
the_flying_circus()

print 'Welcome to the Pig Latin Translator!'

original = raw_input("Enter a word:")

if len(original) > 0:
    print original
else:
    print "empty"


////
pyg = 'ay'

original = raw_input('Enter a word:')

if len(original) > 0 and original.isalpha():
    word = original.lower()
    first = word[0]
    new_word = word + first + pyg
    new_word = new_word[1:len(new_word)]
    print new_word
else:
    print 'empty'

def spam():
    """Prints 'Hello World!' to the console."""
    print "Eggs!"

# Define the spam function above this line.
spam()

def square(n):
    """Returns the square of a number."""
    squared = n**2
    print "%d squared is %d." % (n, squared)
    return squared

n = 10
square(n)


def power(base, exponent):  # Add your parameters here!
    result = base**exponent
    print "%d to the power of %d is %d." % (base, exponent, result)

power(37,4)  # Add your arguments here!

def one_good_turn(n):
    return n + 1
def deserves_another(n):
    return one_good_turn(n) + 2


def cube(number):
    return number*number*number
    
def by_three(number):
    if number % 3 == 0:
        return cube(number)
    else:
        return False

# Ask Python to print sqrt(25) on line 3.
import math
print math.sqrt(25)

# Import *just* the sqrt function from math on line 3!
from math import sqrt

from math import *

import math            # Imports the math module
everything = dir(math) # Sets everything to a list of things from math
print everything       # Prints 'em all!

def biggest_number(*args):
    print max(args)
    return max(args)
    
def smallest_number(*args):
    print min(args)
    return min(args)

def distance_from_zero(arg):
    print abs(arg)
    return abs(arg)


biggest_number(-10, -5, 5, 10)
smallest_number(-10, -5, 5, 10)
distance_from_zero(-10)

def shut_down(s):
    if s == "yes"
        return "Shutting down"
    elif s == "no"
        return "Shutdown aborted"
    else
        return "sorry"

def distance_from_zero(a):
    if type(a) == int or type(a) == float:
        return abs(a)
    else: 
        return "Nope"